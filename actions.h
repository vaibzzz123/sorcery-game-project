#ifndef _ACTIONS_HEADER_
#define _ACTIONS_HEADER_
#include "zone.h"
#include "player.h"
#include <memory>
#include <vector>
#include <iostream>

void draw(std::shared_ptr<Player> p, std::shared_ptr<Zone> h);

bool moveZone(std::shared_ptr<Zone> from, int fromIndex, std::shared_ptr<Zone> to);

#endif
