#include "silence.h"
using namespace std;

Silence:: Silence(): Enchantment{"Silence", 0} {}

string Silence:: effect() {
  return "";
}

string Silence:: getDescription() {
  if (monster) monster->getDescription();
  return "Enchanted minion cannot use abilities";
}

string Silence:: getTrueDescription() {
  return "Enchanted minion cannot use abilities";
}
