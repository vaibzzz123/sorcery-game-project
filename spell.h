#ifndef _SPELL_HEADER_
#define _SPELL_HEADER_
#include "card.h"

class Spell: public Card {
public:
  Spell(int c, std::string name);
};

#endif
