#ifndef _HAND_HEADER_
#define _HAND_HEADER_
#include "zone.h"

class Hand: public Zone {
    const int cap = 5;
  public:
    Hand();
    void init() override;
};

#endif
