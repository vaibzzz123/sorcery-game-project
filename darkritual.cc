#include "darkritual.h"
using namespace std;

DarkRitual:: DarkRitual(): Ritual{"Dark Ritual", 1, 5, 0} {}

string DarkRitual:: effect() {
  return "startturn friendly gainmagic 1";
}

string DarkRitual:: getDescription(){
  return "At the start of turn, gain 1 magic";
}
