#include "grid.h"
using namespace std;

Grid:: Grid(): occ(nullptr), placed{false} {}

void Grid:: placeCard(shared_ptr<Card> c) {
  occ = c;
  placed = true;
}

shared_ptr<Card> Grid:: removeCard() {
  shared_ptr<Card> p = occ;
  occ = nullptr;
  placed = false;
  return p;
}

bool Grid:: isEmpty() {
  return !(placed);
}

shared_ptr<Card> Grid:: myCard() {
  return occ;
}
