#include "banish.h"
using namespace std;

Banish:: Banish(): Spell{2, "Banish"} {}

string Banish:: effect() {
  return "destroy one";
}

string Banish:: getDescription() {
  return "Destroy target minion or ritual";
}
