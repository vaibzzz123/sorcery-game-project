#ifndef _BANISH_HEADER_
#define _BANISH_HEADER_
#include "spell.h"

class Banish: public Spell {
public:
  Banish();
  std::string effect() override;
  std::string getDescription() override;
};

#endif
