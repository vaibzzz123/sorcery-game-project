#include "battleground.h"
using namespace std;

Battleground:: Battleground(): oneHand{make_shared<Hand>()}, oneBoard{make_shared<Board>()}, oneGrave{make_shared<Grave>()}, twoHand{make_shared<Hand>()}, twoBoard{make_shared<Board>()}, twoGrave{make_shared<Grave>()} {
  oneHand->init();
  oneBoard->init();
  oneGrave->init();
  twoHand->init();
  twoBoard->init();
  twoGrave->init();
}

shared_ptr<Zone> Battleground:: getOneHand() {
  return oneHand;
}

shared_ptr<Zone> Battleground:: getOneBoard(){
  return oneBoard;
}

shared_ptr<Zone> Battleground:: getOneGrave(){
  return oneGrave;
}

shared_ptr<Zone> Battleground:: getTwoHand(){
  return twoHand;
}
shared_ptr<Zone> Battleground:: getTwoBoard(){
  return twoBoard;
}

shared_ptr<Zone> Battleground:: getTwoGrave(){
  return twoGrave;
}
