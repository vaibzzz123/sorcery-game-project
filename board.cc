#include "board.h"
using namespace std;

Board :: Board(): Zone(), rit{make_shared<Grid>()} {}

void Board:: init() {
  for (int i = 0; i < cap; i++) {
    auto p = make_shared<Grid>();
    layout.emplace_back(p);
  }
}

bool Board:: place(shared_ptr<Card> c) {
  if (c->getType() == "Ritual") {
    rit->placeCard(c);
    return true;
  } else {
    if (isFull()) {
      return false;
    }
    if (c == nullptr) {
      return false;
    }
    int index = firstEmpty();
    layout[index]->placeCard(c);
    return true;
  }
}


shared_ptr<Card> Board:: getRit() {
  return rit->myCard();
}

void Board:: removeRit() {
  rit->removeCard();
}
