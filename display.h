#ifndef _DISPLAY_HEADER_
#define _DISPLAY_HEADER_
#include <iostream>
#include <memory>
#include "battleground.h"
#include "player.h"
#include "ascii_graphics.h"

class Display {
  std::shared_ptr<Battleground> bg;
  std::shared_ptr<Player> playerOne;
  std::shared_ptr<Player> playerTwo;
  int active_player;
  int oneHealth;
  int twoHealth;
  int inspect_minion_index;
public:
  Display(std::shared_ptr<Battleground> bg, std::shared_ptr<Player> playerOne,
          std::shared_ptr<Player> playerTwo, int active_player, int one, int two,
          int inspect_minion_index = 0);
  void displayBoard() const;
  void displayHand() const;
  void displayMinion() const;
};

#endif
