#ifndef _BONEGOLEM_HEADER_
#define _BONEGOLEM_HEADER_
#include "minion.h"

class BoneGolem: public Minion {
public:
  BoneGolem();
  std::string effect() override;
  std::string getDescription() override;
};

#endif
