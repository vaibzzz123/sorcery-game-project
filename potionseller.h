#ifndef _POTIONSELLER_HEADER_
#define _POTIONSELLER_HEADER_
#include "minion.h"

class PotionSeller: public Minion {
public:
  PotionSeller();
  std::string effect() override;
  std::string getDescription() override;
};

#endif
