#ifndef _STANDSTILL_HEADER_
#define _STANDSTILL_HEADER_
#include "ritual.h"

class Standstill: public Ritual {
public:
  Standstill();
  std::string effect() override;
  std::string getDescription() override;
};

#endif
