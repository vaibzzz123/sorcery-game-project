#include "player.h"
using namespace std;

Player:: Player(): name{""}, myDeck{make_shared<Deck>()}, magic{3} {}

Player:: Player(string n): name{n}, myDeck{make_shared<Deck>()}, magic{3} {}

string Player:: getName() const {
  return name;
}

void Player:: setName(const string name) {
  this->name = name;
}


void Player::shuffleDeck(int shift) {
  myDeck->shuffle(shift);
}

void Player:: loadDeck(const string fileName) {
  myDeck->setDeck(fileName);
}

shared_ptr<Card> Player:: draw() {
  return myDeck->draw();
}

int Player:: getMagic() const {
  return magic;
}

void Player:: useMagic(int i) {
  magic = magic - i;
  if (magic <= 0) magic = 0;
}

void Player:: gainMagic(int i) {
  magic = magic + i;
}
