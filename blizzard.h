#ifndef _BLIZZARD_HEADER_
#define _BLIZZARD_HEADER_
#include "spell.h"

class Blizzard: public Spell {
public:
  Blizzard();
  std::string effect() override;
  std::string getDescription() override;
};

#endif
