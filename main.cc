#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include "helpmessage.h"
#include "player.h"
#include "battleground.h"
#include "actions.h"
#include "turn.h"
#include "display.h"
using namespace std;

int main(int argc, char const *argv[]) {

  string oneName = "Player One";
  string twoName = "Player Two";
  string filename = "";

  bool _testing_ = false;
  string deckOne = "default.deck";
  string deckTwo = "default.deck";
  for (int i = 1; i <= argc-1; i++) { //COMMAND LINE ARGUMENTS
    string command {argv[i]};
    if (command == "-deck1") {
      i++;
      string filename {argv[i]};
      deckOne = filename;
      cout << "Deck One: " << filename << endl;
    } else if (command == "-deck2") {
      i++;
      string filename {argv[i]};
      deckTwo = filename;
      cout << "Deck Two: " << filename << endl;
    } else if (command == "-init") {
      i++;
      filename = argv[i];
    } else if (command == "-testing") {
      _testing_ = true;
      continue;
    } else if (command == "-graphics") {
      cout << "Not Implemented" << endl;
    }
  }// COMMAND LINE ARGUMENTS

  ifstream fs(filename);
  string s;
  cout << "Enter Player One's Name" << endl;
  if (fs >> s) {
    oneName = s;
  } else {
    cin >> oneName;
  }
  cout << "Enter Player Two's Name" << endl;
  if (fs >> s) {
    twoName = s;
  } else {
    cin >> twoName;
  }
  auto one = make_shared<Player>(oneName);
  auto two = make_shared<Player>(twoName);
  bool ongoing = true;
  one->loadDeck(deckOne);
  two->loadDeck(deckTwo);
  auto playfield = make_shared<Battleground>();
  auto turn = make_shared<Turn>(one, two, playfield);

  if (!(_testing_)) {
    one->shuffleDeck(-2);
    two->shuffleDeck(2);
  }
  turn->gameStart();
  one->gainMagic(1);
  cout << "Welcome to the game of Sorcery, type \"help\" for a list of commands" << endl;

  while (ongoing) {
    if (cin.eof()) break;
    string command;
    string c;
    if (getline(fs, c)) {
    } else {
      getline(cin, c);
    }
    istringstream ss(c);
    ss >> command;
    if (command == "help") {
      printHelp();
    } else if (command == "end") {
      turn->endTurn();
      turn->startTurn();
    } else if (command == "quit") {
      ongoing = false;
    } else if (command == "attack") {
      int i, j;
      ss >> i;
      i--;
      shared_ptr<Card> actionCheck;
      if (turn->active() == 1) {
        actionCheck = playfield->getOneBoard()->getCard(i);
      } else {
        actionCheck = playfield->getTwoBoard()->getCard(i);
      }
      if (actionCheck == nullptr) continue;
      if (actionCheck->getAction() <= 0) {
        cout << "This minion cannot move anymore this turn" << endl;
        continue;
      }
      if(!(ss >> j)) {
        turn->attackPlayer(i);
      } else {
        j--;
        turn->attackMinion(i, j);
      }
      actionCheck->useAction();
    } else if (command == "use") {
      int i, t, p;
      ss >> i;
      i--;
      shared_ptr<Card> actionCheck;
      if (turn->active() == 1) {
        actionCheck = playfield->getOneBoard()->getCard(i);
      } else {
        actionCheck = playfield->getTwoBoard()->getCard(i);
      }
      if (actionCheck == nullptr) continue;
      if (actionCheck->getAction() <= 0) {
        cout << "This minion cannot move anymore this turn" << endl;
        continue;
      }
      shared_ptr<Card> magicCheck;
      if (turn->active() == 1) {
        magicCheck = playfield->getOneBoard()->getCard(i);
        if (magicCheck->getEffectCost() > one->getMagic() && (!(_testing_))) {
          cout << one->getName() << " does not have enough magic" << endl;
          continue;
        }
      } else {
        magicCheck = playfield->getTwoBoard()->getCard(i);
        if (magicCheck->getEffectCost() > two->getMagic() && (!(_testing_))) {
          cout << two->getName() << " does not have enough magic" << endl;
          continue;
        }
      }
      if (!(ss >> p)) {
        if (i < 0) continue;
        if (turn->active() == 1){
          auto temp = playfield->getOneBoard()->getCard(i);
          if (temp == nullptr) continue;
          if (turn->monsterNonTarget(i)) {
            one->useMagic(magicCheck->getEffectCost());
            actionCheck->useAction();
          }
        } else {
          auto temp = playfield->getTwoBoard()->getCard(i);
          if (temp == nullptr) continue;
          if (turn->monsterNonTarget(i)) {
            two->useMagic(magicCheck->getEffectCost());
            actionCheck->useAction();
          }
        }
      } else {
        if (!(ss >> t)) continue;
        t--;
        if (p <= 0 || t < 0) continue;
        if (turn->active() == 1){
          auto temp = playfield->getOneBoard()->getCard(i);
          if (temp == nullptr) continue;
          if (turn->monsterTarget(i, p, t)) {
            one->useMagic(magicCheck->getCost());
            actionCheck->useAction();
          }
        } else {
          auto temp = playfield->getTwoBoard()->getCard(i);
          if (temp == nullptr) continue;
          if (turn->monsterTarget(i, p, t)) {
            two->useMagic(magicCheck->getCost());
            actionCheck->useAction();
          }
        }
      }
    } else if (command == "describe") {
      int i;
      ss >> i;
      i--;
      if (i < 0 || i > 4) {
        cout << "out of range" << endl;
        continue;
      }
      auto d = make_shared<Display>(playfield, one, two, turn->active(), turn->getOneHealth(), turn->getTwoHealth(), i);
      d->displayMinion();
    } else if (command == "hand") {
      auto d = make_shared<Display>(playfield, one, two, turn->active(), turn->getOneHealth(), turn->getTwoHealth());
      d->displayHand();
    } else if (command == "board") {
      auto d = make_shared<Display>(playfield, one, two, turn->active(), turn->getOneHealth(), turn->getTwoHealth());
      d->displayBoard();
    } else if (command == "play") {
      int i, p, t;
      ss >> i;
      if (i <= 0 || i > 5) continue;
      i--;
      shared_ptr<Card> magicCheck;
      if (turn->active() == 1) {
        magicCheck = playfield->getOneHand()->getCard(i);
        if (magicCheck->getCost() > one->getMagic() && (!(_testing_))) {
          cout << one->getName() << " does not have enough magic" << endl;
          continue;
        }
      } else {
        magicCheck = playfield->getTwoHand()->getCard(i);
        if (magicCheck->getCost() > two->getMagic() && (!(_testing_))) {
          cout << two->getName() << " does not have enough magic" << endl;
          continue;
        }
      }
      if (!(ss >> p)) {
        shared_ptr<Card> temp;
        if (turn->active() == 1){
          temp = playfield->getOneHand()->getCard(i);
          if (temp == nullptr) continue;;
          if (temp->getType() == "Minion") {
            if (turn->summon(temp, i)) one->useMagic(temp->getCost());
          } else if (temp->getType() == "Spell") {
            if (turn->nonTargetEffect(i)) {
              one->useMagic(temp->getCost());
              playfield->getOneHand()->remove(i);
            }
          } else if (temp->getType() == "Ritual"){
            one->useMagic(temp->getCost());
            playfield->getOneBoard()->place(temp);
            playfield->getOneHand()->remove(i);
          }
        } else {
          temp = playfield->getTwoHand()->getCard(i);
          if (temp == nullptr) continue;
          if (temp->getType() == "Minion") {
            if (turn->summon(temp, i)) two->useMagic(temp->getCost());
          } else if (temp->getType() == "Spell") {
            if (turn->nonTargetEffect(i)) {
              two->useMagic(temp->getCost());
              playfield->getTwoHand()->remove(i);
            }
          } else if (temp->getType() == "Ritual") {
            two->useMagic(temp->getCost());
            playfield->getTwoBoard()->place(temp);
            playfield->getTwoHand()->remove(i);
          }
        }
      } else {
        char r;
        if(!(ss >> r)) continue;
        t = r;
        t = t - 49;
        if (p <= 0 || t < 0) continue;
        if (turn->active() == 1){
          auto temp = playfield->getOneHand()->getCard(i);
          if (temp == nullptr) continue;
          if (temp->getType() == "Spell") {
            if (turn->targetEffect(i, p, t)) {
              one->useMagic(temp->getCost());
              playfield->getOneHand()->remove(i);
            }
          } else if (temp->getType() == "Enchantment") {
            shared_ptr<Zone> target;
            if (p == 1) {
              target = playfield->getOneBoard();
            } else if (p == 2) {
              target = playfield->getTwoBoard();
            } else {
              continue;
            }
            if(target->getCard(t) == nullptr) continue;
            one->useMagic(temp->getCost());
            temp->wrap(target->getCard(t));
            target->remove(t);
            target->replace(temp, t);
            playfield->getOneHand()->remove(i);
          }
        } else {
          auto temp = playfield->getTwoHand()->getCard(i);
          if (temp == nullptr) continue;
          if (temp->getType() == "Spell") {
            if (turn->targetEffect(i, p, t)) {
              two->useMagic(temp->getCost());
              playfield->getTwoHand()->remove(i);
            }
          } else if (temp->getType() == "Enchantment") {
            shared_ptr<Zone> target;
            if (p == 1) {
              target = playfield->getOneBoard();
            } else if (p == 2) {
              target = playfield->getTwoBoard();
            } else {
              continue;
            }
            if(target->getCard(t) == nullptr) continue;
            two->useMagic(temp->getCost());
            temp->wrap(target->getCard(t));
            target->remove(t);
            target->replace(temp, t);
            playfield->getTwoHand()->remove(i);
          }
        }
      }
    }

    // -testing flag
    if (_testing_) {
      if (command == "draw") {
        turn->drawCard();
      } else if (command == "discard") {
        int i;
        ss >> i;
        i--;
        if (turn->active() == 1) {
          playfield->getOneHand()->remove(i);
        } else {
          playfield->getTwoHand()->remove(i);
        }
      }
    }

    if(command == "shuffle") {
      if(turn->active() == 1) {
        one->shuffleDeck(0);
      }
      else {
        two->shuffleDeck(0);
      }
    }
    if (turn->getOneHealth() <= 0) {
      cout << two->getName() << " WINS!" << endl;
      ongoing = false;
    } else if (turn->getTwoHealth() <= 0) {
      cout << one->getName() << " WINS!" << endl;
      ongoing = false;
    }
  }
}
