#include "actions.h"
using namespace std;

void draw(shared_ptr<Player> p, shared_ptr<Zone> h) {
  if (h->isFull()) {
    cout << "Hand is FULL!!!" << endl;
  } else {
    h->place(p->draw());
  }
}

bool moveZone(shared_ptr<Zone> from, int fromIndex, shared_ptr<Zone> to) {
  if (to->isFull()) {
    cout << "The target zone is already full" << endl;
    return false;
  } else if (from->getCard(fromIndex) == nullptr) {
    cout << "No card there" << endl;
    return false;
  }
  to->place(from->remove(fromIndex));
  return true;
}
