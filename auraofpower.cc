#include "auraofpower.h"
using namespace std;

AuraOfPower:: AuraOfPower(): Ritual{"Aura of Power", 1, 4, 1} {}

string AuraOfPower:: effect() {
  return "summoneffect friendly bufftarget 1 1";
}

string AuraOfPower:: getDescription() {
  return "Whenever a minon enters play under your control, it gains +1/+1";
}
