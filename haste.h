#ifndef _HASTE_HEADER_
#define _HASTE_HEADER_
#include "enchantment.h"

class Haste: public Enchantment {
public:
  Haste();
  int getAction() override;
  std::string getDescription() override;
  std::string getTrueDescription() override;
};

#endif
