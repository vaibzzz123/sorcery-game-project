#ifndef _RAISE_DEAD_
#define _RAISE_DEAD_
#include "spell.h"

class RaiseDead: public Spell {
public:
  RaiseDead();
  std::string effect() override;
  std::string getDescription() override;
};

#endif
