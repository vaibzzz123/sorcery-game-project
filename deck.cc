#include "deck.h"
#include <cstdlib>
#include <ctime>
using namespace std;

Deck:: Deck(): cardList() {}

void Deck:: setDeck(const string file) {
  ifstream fs(file);
  string dummy;
  while(getline(fs, dummy)) {
    //this is a weird section because filestream messes up my string
    istringstream ss(dummy);
    string temp;
    string result;
    ss >> temp;
    result = temp;
    while (ss >> temp) {
      result = result + " " + temp;
    }
    //exact copy of dummy, since dummy prints overlaps?? anyway ignore this section

    if (result == "Air Elemental") {
      cardList.emplace_back(make_shared<AirElemental>());
    } else if (result == "Earth Elemental") {
      cardList.emplace_back(make_shared<EarthElemental>());
    } else if (result == "Bone Golem") {
      cardList.emplace_back(make_shared<BoneGolem>());
    } else if (result == "Fire Elemental") {
      cardList.emplace_back(make_shared<FireElemental>());
    } else if (result == "Potion Seller") {
      cardList.emplace_back(make_shared<PotionSeller>());
    } else if (result == "Novice Pyromancer") {
      cardList.emplace_back(make_shared<NovicePyromancer>());
    } else if (result == "Apprentice Summoner") {
      cardList.emplace_back(make_shared<ApprenticeSummoner>());
    } else if (result == "Master Summoner") {
      cardList.emplace_back(make_shared<MasterSummoner>());
    } else if (result == "Banish") {
      cardList.emplace_back(make_shared<Banish>());
    } else if (result == "Unsummon") {
      cardList.emplace_back(make_shared<Unsummon>());
    } else if (result == "Raise Dead") {
      cardList.emplace_back(make_shared<RaiseDead>());
    } else if (result == "Blizzard") {
      cardList.emplace_back(make_shared<Blizzard>());
    } else if (result == "Aura of Power") {
      cardList.emplace_back(make_shared<AuraOfPower>());
    } else if (result == "Standstill") {
      cardList.emplace_back(make_shared<Standstill>());
    } else if (result == "Recharge") {
      cardList.emplace_back(make_shared<Recharge>());
    } else if (result == "Giant Strength") {
      cardList.emplace_back(make_shared<GiantStrength>());
    } else if (result == "Enrage") {
      cardList.emplace_back(make_shared<Enrage>());
    } else if (result == "Silence") {
      cardList.emplace_back(make_shared<Silence>());
    } else if (result == "Disenchant") {
      cardList.emplace_back(make_shared<Disenchant>());
    } else if (result == "Dark Ritual") {
      cardList.emplace_back(make_shared<DarkRitual>());
    } else if (result == "Haste") {
      cardList.emplace_back(make_shared<Haste>());
    } else if (result == "Magic Fatigue") {
      cardList.emplace_back(make_shared<MagicFatigue>());
    }
    //cardList.emplace_back(make_shared<Card>(result));

    //add more cards as we implement more cards;

  }
}

void Deck:: shuffle(int shift) {
  int length = this->cardList.size();
  srand(time(0)+shift);
  for(int i = 0; i < length; ++i) {
    int shuffleIndex = rand() % length;
    if(i != shuffleIndex) {
      auto temp = this->cardList[i];
      this->cardList[i] = this->cardList[shuffleIndex];
      this->cardList[shuffleIndex] = temp;
    }
  }
}

shared_ptr<Card> Deck:: draw() {
  if (cardList.size() <= 0) {
    cout << "DECK EMPTY!" << endl;
    return nullptr;
  }
  //draw first card in the deck
  shared_ptr<Card> p = cardList.at(0);
  cardList.erase(cardList.begin());

  //draw last card in the deck
  //shared_ptr<Card> p = cardList.back();
  //cardList.pop_back();
  return p;
}
