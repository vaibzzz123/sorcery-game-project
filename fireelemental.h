#ifndef _FIREELEMENTAL_HEADER_
#define _FIREELEMENTAL_HEADER_
#include "minion.h"

class FireElemental: public Minion {
public:
  FireElemental();
  std::string effect() override;
  std::string getDescription() override;
};

#endif
