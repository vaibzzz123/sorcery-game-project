#include "novicepyromancer.h"
using namespace std;

NovicePyromancer:: NovicePyromancer(): Minion{0, 1, 1, "Novice Pyromancer"} {
  effectCost = 1;
}

int NovicePyromancer:: getEffectCost() {
  return effectCost;
}

string NovicePyromancer:: effect() {
  return "damage one 1";
}

string NovicePyromancer:: getDescription() {
  return "Deal 1 damage to target minion";
}
