#ifndef _RECHARGE_HEADER_
#define _RECHARGE_HEADER_
#include "spell.h"

class Recharge:public Spell{
public:
  Recharge();
  std::string effect() override;
  std::string getDescription() override;
};

#endif
