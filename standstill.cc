#include "standstill.h"
using namespace std;

Standstill:: Standstill(): Ritual{"Standstill", 2, 4, 3} {}

string Standstill:: effect() {
  return "summoneffect all destroytarget";
}

string Standstill:: getDescription() {
  return "Whenever a minion enters play, destroy it";
}
