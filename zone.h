#ifndef _ZONE_HEADER_
#define _ZONE_HEADER_
#include <vector>
#include <memory>
#include <iostream>
#include "grid.h"

class Zone {
protected:
  std::vector<std::shared_ptr<Grid> > layout;
public:
  Zone();
  virtual void init() = 0;
  int firstEmpty() const;
  virtual bool isFull();
  virtual bool place(std::shared_ptr<Card> c); //false if unsuccessful(full board)
  virtual std::shared_ptr<Card> remove(int i);
  std::shared_ptr<Card> getCard(int i);
  virtual std::shared_ptr<Card> getRit();
  virtual void removeRit();
  virtual int getCount();
  void replace(std::shared_ptr<Card> c, int i); //  do not use unless spot is empty
};
#endif
