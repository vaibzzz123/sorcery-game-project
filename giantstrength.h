#ifndef _GIANTSTRENGTH_HEADER_
#define _GIANTSTRENGTH_HEADER_
#include "enchantment.h"

class GiantStrength: public Enchantment {
public:
  GiantStrength();
  int getAtt() override;
  int getDef() override;
  std::string getDescription() override;
  std::string getTrueDescription() override;
};

#endif
