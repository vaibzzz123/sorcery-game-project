#ifndef _MINIONS_HEADER_
#define _MINIONS_HEADER_
#include "card.h"

class Minion: public Card{
protected:
  int attack;
  int defense;
  int action;
public:
  Minion(int att, int def, int c, std::string name);
  int getAtt() override;
  int getDef() override;
  void setDef(int i) override;
  void damage(int i) override;
  void buffDef(int i) override;
  void buffAtt(int i) override;
  void getAttacked(Card &c) override;
  void attackMinion(Card &m) override;
  void setAction(int i) override;
  void useAction() override;
  int getAction() override;
};

#endif
