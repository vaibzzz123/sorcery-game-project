#ifndef _PLAYER_HEADER_
#define _PLAYER_HEADER_
#include <string>
#include <iostream>
#include <memory>
#include "deck.h"

class Player {
protected:
  std::string name;
  std::shared_ptr<Deck> myDeck;
  int magic;
public:
  Player();
  Player(std::string n);
  std::string getName() const;
  void setName(const std::string name);
  void loadDeck(const std::string fileName);
  void shuffleDeck(int shift);
  std::shared_ptr<Card> draw();
  int getMagic() const;
  void useMagic(int i);
  void gainMagic(int i);
};

#endif
