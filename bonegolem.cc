#include "bonegolem.h"
using namespace std;

BoneGolem:: BoneGolem(): Minion{1, 3, 2, "Bone Golem"} {}

string BoneGolem:: effect() {
  return "miniondeath buff 1 1";
}

string BoneGolem:: getDescription() {
  return "Gain +1/+1 whenever a minion leaves play";
}
