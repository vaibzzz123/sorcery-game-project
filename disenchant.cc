#include "disenchant.h"
using namespace std;

Disenchant:: Disenchant(): Spell{1, "Disenchant"} {}

string Disenchant:: effect() {
  return "disenchant one";
}

string Disenchant:: getDescription() {
  return "Destroy the top enchantment on target minion";
}
