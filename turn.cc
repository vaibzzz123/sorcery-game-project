#include "turn.h"
using namespace std;

Turn:: Turn(shared_ptr<Player> one, shared_ptr<Player> two, shared_ptr<Battleground> f): activePlayer{1}, oneHealth{20}, twoHealth{20}, playerOne{one}, playerTwo{two}, field{f} {}

void Turn:: drawCard() {
  if (activePlayer == 1) {
    if (!field->getOneHand()->isFull()) {
      draw(playerOne, field->getOneHand());
    }
  } else {
    if (!field->getTwoHand()->isFull()) {
      draw(playerTwo, field->getTwoHand());
    }
  }
}

void Turn:: startTurn() {
  drawCard();
  shared_ptr<Zone> target;
  if (activePlayer == 1) {
    target = field->getOneBoard();
    playerOne->gainMagic(1);
  } else {
    target = field->getTwoBoard();
    playerTwo->gainMagic(1);
  }
  for (int i = 0; i <= 4; i++) {
    if (target->getCard(i) == nullptr) continue;
    target->getCard(i)->setAction(1);
  }
  startTrigger();
}

void Turn:: startTrigger() {
  shared_ptr<Zone> first;
  shared_ptr<Zone> second;
  if (activePlayer == 1) {
    first = field->getOneBoard();
    second = field->getTwoBoard();
  } else {
    first = field->getTwoBoard();
    second = field->getOneBoard();
  }
  shared_ptr<Card> rit;
  rit = first->getRit();
  if (!(rit == nullptr)) {
    if (rit->getEffectCost() <= rit->getTotalMana()) {
      string s = rit->effect();
      istringstream ss(s);
      string word;
      ss >> word;
      if (word == "startturn") {
        ss >> word;
        if (word == "friendly") {
          ss >> word;
          if (word == "gainmagic") {
            int amount;
            ss >> amount;
            if (activePlayer == 1) {
              playerOne->gainMagic(amount);
            } else {
              playerTwo->gainMagic(amount);
            }
            rit->useEffect();
          }
        }
      }
    }
  }
}

void Turn:: endTurn() {
  endTrigger();
  if (activePlayer == 1) {
    activePlayer = 2;
  } else {
    activePlayer = 1;
  }
}

void Turn:: endTrigger() {
  shared_ptr<Zone> first;
  shared_ptr<Zone> second;
  if (activePlayer == 1) {
    first = field->getOneBoard();
    second = field->getTwoBoard();
  } else {
    first = field->getTwoBoard();
    second = field->getOneBoard();
  }
  for (int i = 0; i <= 4; i++) {
    if (first->getCard(i) == nullptr) continue;
    string s = first->getCard(i)->effect();
    istringstream ss(s);
    string word;
    ss >> word;
    if (word == "endturn") {
      ss >> word;
      if (word == "friendly") {
        ss >> word;
        if (word == "buffall") {
          int att, def;
          ss >> att;
          ss >> def;
          for (int k = 0; k <= 4; k++) {
            if (first->getCard(k) == nullptr) continue;
            first->getCard(k)->buffAtt(att);
            first->getCard(k)->buffDef(def);
          }
        }
      }
    }
  }
  for (int i = 0; i <= 4; i++) {
    if (second->getCard(i) == nullptr) continue;
    string s = second->getCard(i)->effect();
    istringstream ss(s);
    string word;
    ss >> word;
    if (word == "endturn") {
      ss >> word;
      if (word == "friendly") {
        continue;
      }
    }
  }
}

bool Turn:: die(shared_ptr<Zone> z, int i, shared_ptr<Zone> grave) {
  if (z->getCard(i)) {
    while (z->getCard(i)->getMinion()) {
      z->replace(z->getCard(i)->getMinion(), i);
    }
  }
  if (!(moveZone(z, i, grave))) {
    return false;
  }
  dieTrigger();
  return true;
}

void Turn:: dieTrigger() {
  shared_ptr<Zone> first;
  shared_ptr<Zone> second;
  if (activePlayer == 1) {
    first = field->getOneBoard();
    second = field->getTwoBoard();
  } else {
    first = field->getTwoBoard();
    second = field->getOneBoard();
  }
  for (int i = 0; i <= 4; i++) {
    if (first->getCard(i) == nullptr) continue;
    string s = first->getCard(i)->effect();
    istringstream ss(s);
    string word;
    ss >> word;
    if (word == "miniondeath") {
      ss >> word;
      if (word == "buff") {
        int att, def;
        ss >> att;
        ss >> def;
        first->getCard(i)->buffAtt(att);
        first->getCard(i)->buffDef(def);
      }
    }
  }
  for (int i = 0; i <= 4; i++) {
    if (second->getCard(i) == nullptr) continue;
    string s = second->getCard(i)->effect();
    istringstream ss(s);
    string word;
    ss >> word;
    if (word == "miniondeath") {
      ss >> word;
      if (word == "buff") {
        int att, def;
        ss >> att;
        ss >> def;
        second->getCard(i)->buffAtt(att);
        second->getCard(i)->buffDef(def);
      }
    }
  }
}

bool Turn:: summon(std::shared_ptr<Card> c, int i) {
  shared_ptr<Zone> b;
  shared_ptr<Zone> hand;
  if (activePlayer == 1) {
    b = field->getOneBoard();
    hand = field->getOneHand();
  } else {
    b = field->getTwoBoard();
    hand = field->getTwoHand();
  }
  if (!(b->place(c))) {
    return false;
  } else if (i >= 0) {
    hand->remove(i);
  }
  c->setAction(0);
  summonTrigger(c);
  return true;
}

void Turn:: summonTrigger(shared_ptr<Card> c) {
  shared_ptr<Zone> first;
  shared_ptr<Zone> second;
  shared_ptr<Card> rit;
  if (activePlayer == 1) {
    first = field->getOneBoard();
    second = field->getTwoBoard();
  } else {
    first = field->getTwoBoard();
    second = field->getOneBoard();
  }
  for (int i = 0; i <= 4; i++) {
    if (first->getCard(i) == nullptr) continue;
    if (first->getCard(i) == c) continue;
    string s = first->getCard(i)->effect();
    istringstream ss(s);
    string word;
    ss >> word;
    if (word == "summoneffect") {
      ss >> word;
      if (word == "enemy") {
        continue;
      }
    }
  }
  rit = first->getRit();
  if (!(rit == nullptr)) {
    if (rit->getEffectCost() <= rit->getTotalMana()) {
      string s = rit->effect();
      istringstream ss(s);
      string word;
      ss >> word;
      if (word == "summoneffect") {
        ss >> word;
        if (word == "friendly") {
          ss >> word;
          if (word == "bufftarget") {
            int att, def;
            ss >> att >> def;
            c->buffAtt(att);
            c->buffDef(def);
            rit->useEffect();
          }
        } else if (word == "all") {
          ss >> word;
          if (word == "destroytarget") {
            c->setDef(0);
            rit->useEffect();
            divineJudgement();
          }
        }
      }
    }
  }
  for (int i = 0; i <= 4; i++) {
    if (second->getCard(i) == nullptr) continue;
    if (second->getCard(i) == c) continue;
    string s = second->getCard(i)->effect();
    istringstream ss(s);
    string word;
    ss >> word;
    if (word == "summoneffect") {
      ss >> word;
      if (word == "enemy") {
        ss >> word;
        if (word == "damagesummon") {
          int amount;
          ss >> amount;
          c->damage(amount);
          divineJudgement();
        }
      }
    }
  }
  rit = second->getRit();
  if (!(rit == nullptr)) {
    if (rit->getEffectCost() <= rit->getTotalMana()) {
      string s = rit->effect();
      istringstream ss(s);
      string word;
      ss >> word;
      if (word == "summoneffect") {
        ss >> word;
        if (word == "all") {
          ss >> word;
          if (word == "destroytarget") {
            if (c->getDef() != 0) {
              c->setDef(0);
              rit->useEffect();
              divineJudgement();
            }
          }
        }
      }
    }
  }
}

int Turn:: active() {
  return activePlayer;
}

int & Turn:: getOneHealth() {
  return oneHealth;
}

int & Turn:: getTwoHealth() {
  return twoHealth;
}

shared_ptr<Player> Turn:: getPlayerOne() {
  return playerOne;
}

shared_ptr<Player> Turn:: getPlayerTwo() {
  return playerTwo;
}

shared_ptr<Battleground> Turn:: getGround() {
  return field;
}

void Turn:: judgementDay(shared_ptr<Zone> z, int i, shared_ptr<Zone> grave) {
  if (z->getCard(i)->getDef() <= 0) {
    die(z, i, grave);
  }
}

void Turn:: attackPlayer(int i) {
  if (activePlayer == 1) {
    if (field->getOneBoard()->getCard(i) == nullptr) {
      cout << "No one is there to attack" << endl;
      return;
    }
    int attack = field->getOneBoard()->getCard(i)->getAtt();
    twoHealth = twoHealth - attack;
    if (twoHealth <= 0) {
      cout << "Player two is dead" << endl;
      twoHealth = 0;
    }
  } else {
    if (field->getTwoBoard()->getCard(i) == nullptr) {
      cout << "No one is there to attack" << endl;
      return;
    }
    int attack = field->getTwoBoard()->getCard(i)->getAtt();
    oneHealth = oneHealth - attack;
    if (oneHealth <= 0) {
      cout << "Player one is dead" << endl;
      oneHealth = 0;
    }
  }
}

void Turn:: attackMinion(int i, int j) {
  auto attacker = make_shared<Card>();
  auto attacked = make_shared<Card>();
  if (activePlayer == 1) {
    attacker = field->getOneBoard()->getCard(i);
    if (attacker == nullptr) {
      cout << "No one is there to attack" << endl;
      return;
    }
    attacked = field->getTwoBoard()->getCard(j);
    if (attacked == nullptr) {
      cout << "You are not attacking anything" << endl;
      return;
    }
    attacked->getAttacked(*attacker);
    attacker->getAttacked(*attacked);
    judgementDay(field->getOneBoard(), i, field->getOneGrave());
    judgementDay(field->getTwoBoard(), j, field->getTwoGrave());
  } else {
    attacker = field->getTwoBoard()->getCard(i);
    if (attacker == nullptr) {
      cout << "No one is there to attack" << endl;
      return;
    }
    attacked = field->getOneBoard()->getCard(j);
    if (attacked == nullptr) {
      cout << "You are not attacking anything" << endl;
      return;
    }
    attacked->getAttacked(*attacker);
    attacker->getAttacked(*attacked);
    judgementDay(field->getOneBoard(), j, field->getOneGrave());
    judgementDay(field->getTwoBoard(), i, field->getTwoGrave());
  }
}

bool Turn:: targetEffect(int i, int p, int t) {
  string effect;
  if (activePlayer == 1) {
    effect = field->getOneHand()->getCard(i)->effect();
  } else {
    effect = field->getTwoHand()->getCard(i)->effect();
  }
  if (effect == "") {
    cout << "no effect" << endl;
    return false;
  }
  istringstream ss(effect);
  string word;
  ss >> word;
  if (word == "destroy") {
    ss >> word;
    if (word == "one") {
      shared_ptr<Zone> from;
      shared_ptr<Zone> to;
      if (p == 1) {
        from = field->getOneBoard();
        to = field->getOneGrave();
      } else {
        from = field->getTwoBoard();
        to = field->getTwoGrave();
      }
      if (t == 65) { // if its targeting ritual
        if (from->getRit() == nullptr) return false;
        from->removeRit();
        return true;
      }
      if (die(from, t, to)) {
        return true;
      }
      return false;
    }
  } else if (word == "return") {
    ss >> word;
    if (word == "one") {
      shared_ptr<Zone> from;
      shared_ptr<Zone> to;
      if (p == 1) {
        from = field->getOneBoard();
        to = field->getOneHand();
        if (to->isFull()) return false;
      } else {
        from = field->getTwoBoard();
        to = field->getTwoHand();
        if (to->isFull()) return false;
      }
      if (from->getCard(t) == nullptr) return false;
      while(from->getCard(t)->getMinion()) {
        from->replace(from->getCard(t)->getMinion(), t);
      }
      divineJudgement();
      moveZone(from, t, to);
      return true;
    }
  } else if (word == "damage") {
    ss >> word;
    if (word == "one") {
      int i;
      ss >> i;
      if (p == 1)  field->getOneBoard()->getCard(t)->damage(1);
      if (p == 2)  field->getTwoBoard()->getCard(t)->damage(1);
    }
  } else if (word == "disenchant") {
    ss >> word;
    if (word == "one") {
      shared_ptr<Zone> target;
      if (p == 1) target = field->getOneBoard();
      if (p == 2) target = field->getTwoBoard();
      if (target->getCard(t) == nullptr) return false;
      if (target->getCard(t)->getMinion() == nullptr) {
        cout << "There is no enchantment on that monster" << endl;
        return false;
      }
      target->replace(target->getCard(t)->getMinion(), t);
      divineJudgement();
      return true;
    }
  }
  return false;
}

bool Turn:: monsterTarget(int i, int p, int t) {
  string effect;
  if (activePlayer == 1) {
    effect = field->getOneBoard()->getCard(i)->effect();
  } else {
    effect = field->getTwoBoard()->getCard(i)->effect();
  }
  if (effect == "") {
    cout << "no effect" << endl;
    return false;
  }
  istringstream ss(effect);
  string word;
  ss >> word;
  if (word == "damage") {
    ss >> word;
    if (word == "one") {
      int i;
      ss >> i;
      if (p == 1)  field->getOneBoard()->getCard(t)->damage(1);
      if (p == 2)  field->getTwoBoard()->getCard(t)->damage(1);
      divineJudgement();
      return true;
    }
  }
  return false;
}

bool Turn:: nonTargetEffect(int i) {
  string effect;
  if (activePlayer == 1) {
    effect = field->getOneHand()->getCard(i)->effect();
  } else {
    effect = field->getTwoHand()->getCard(i)->effect();
  }
  if (effect == "") {
    cout << "no effect" << endl;
    return false;
  }
  istringstream ss(effect);
  string word;
  ss >> word;
  if (word == "revive") {
    ss >> word;
    if (word == "one") {
      if (activePlayer == 1) {
        if (field->getOneBoard()->isFull()) return false;
        if (field->getOneGrave()->getCount() == 0) return false;
        auto p = field->getOneGrave()->remove(0);
        p->setDef(1);
        summon(p, -1);
        return true;
      } else {
        if (field->getTwoBoard()->isFull()) return false;
        if (field->getTwoGrave()->getCount() == 0) return false;
        auto p = field->getTwoGrave()->remove(0);
        p->setDef(1);
        summon(p, -1);
        return true;
      }
    }
  } else if (word == "damage") {
    ss >> word;
    int amount;
    ss >> amount;
    if (word == "all") {
      shared_ptr<Zone> first;
      shared_ptr<Zone> firstGrave;
      shared_ptr<Zone> second;
      shared_ptr<Zone> secondGrave;
      if (activePlayer == 1) {
        first = field->getOneBoard();
        firstGrave = field->getOneGrave();
        second = field->getTwoBoard();
        secondGrave = field->getTwoGrave();
      } else {
        first = field->getTwoBoard();
        firstGrave = field->getTwoGrave();
        second = field->getOneBoard();
        secondGrave = field->getOneGrave();
      }
      for (int i = 0; i <= 4; i++) {
        auto temp = first->getCard(i);
        if (temp == nullptr) continue;
        temp->damage(amount);
        judgementDay(first, i, firstGrave);
      }
      for (int i = 0; i <= 4; i++) {
        auto temp = second->getCard(i);
        if (temp == nullptr) continue;
        temp->damage(amount);
        judgementDay(second, i, secondGrave);
      }
      return true;
    }
  } else if (word == "ritual") {
    ss >> word;
    if (word == "chargegain") {
      int i;
      ss >> i;
      if (activePlayer == 1) {
        if (field->getOneBoard()->getRit() == nullptr) return false;
        field->getOneBoard()->getRit()->gainMana(i);
        return true;
      } else {
        if (field->getTwoBoard()->getRit() == nullptr) return false;
        field->getTwoBoard()->getRit()->gainMana(i);
        return true;
      }
    }
  }
  return false;
}

bool Turn:: monsterNonTarget(int i) {
  string effect;
  if (activePlayer == 1) {
    effect = field->getOneBoard()->getCard(i)->effect();
  } else {
    effect = field->getTwoBoard()->getCard(i)->effect();
  }
  if (effect == "") {
    cout << "no effect" << endl;
    return false;
  }
  istringstream ss(effect);
  string word;
  ss >> word;
  if (word == "summon") {
    ss >> word;
    if (word == "airelemental") {
      int k;
      ss >> k;
      shared_ptr<Zone> target;
      if (activePlayer == 1) {
        target = field->getOneBoard();
        if (target->isFull()) return false;
      } else {
        target = field->getTwoBoard();
        if (target->isFull()) return false;
      }
      for (int i = 0; i < k; i++) {
        auto temp = make_shared<AirElemental>();
        summon(temp, -1);
        if (target->isFull()) break;
      }
      return true;
    }
  }
  return false;
}

void Turn:: divineJudgement() {
  for (int i = 0; i <= 4; i++) {
    auto oneCard = field->getOneBoard()->getCard(i);
    auto twoCard = field->getTwoBoard()->getCard(i);
    if (oneCard != nullptr) {
      if (oneCard->getDef() <= 0) {
        die(field->getOneBoard(), i, field->getOneGrave());
      }
    }
    if (twoCard != nullptr) {
      if (twoCard->getDef() <= 0) {
        die(field->getTwoBoard(), i, field->getTwoGrave());
      }
    }
  }
}

void Turn:: gameStart() {
  draw(playerOne, field->getOneHand());
  draw(playerOne, field->getOneHand());
  draw(playerOne, field->getOneHand());
  draw(playerOne, field->getOneHand());
  draw(playerOne, field->getOneHand());
  draw(playerTwo, field->getTwoHand());
  draw(playerTwo, field->getTwoHand());
  draw(playerTwo, field->getTwoHand());
  draw(playerTwo, field->getTwoHand());
  draw(playerTwo, field->getTwoHand());
}
