#ifndef _ENRAGE_HEADER_
#define _ENRAGE_HEADER_
#include "enchantment.h"

class Enrage: public Enchantment {
  int prevAttack;
  int prevDefense;
public:
  Enrage();
  int getAtt() override;
  int getDef() override;
  void wrap(std::shared_ptr<Card> c) override;
  std::string getDescription() override;
  std::string getTrueDescription() override;
};

#endif
