#ifndef _RITUAL_HEADER_
#define _RITUAL_HEADER_
#include "card.h"

class Ritual: public Card {
  int effectCost;
  int totalMana;
public:
  Ritual(std::string name, int e, int t, int c);
  void gainMana(int i) override;
  int getEffectCost() override;
  int getTotalMana() override;
  void useEffect() override;
};


#endif
