#ifndef _DECK_HEADER_
#define _DECK_HEADER_
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <memory>
#include <vector>
#include "card.h"
#include "minion.h"
#include "airelemental.h"
#include "earthelemental.h"
#include "bonegolem.h"
#include "fireelemental.h"
#include "potionseller.h"
#include "novicepyromancer.h"
#include "apprenticesummoner.h"
#include "mastersummoner.h"
#include "banish.h"
#include "unsummon.h"
#include "raisedead.h"
#include "blizzard.h"
#include "auraofpower.h"
#include "standstill.h"
#include "recharge.h"
#include "giantstrength.h"
#include "enrage.h"
#include "silence.h"
#include "disenchant.h"
#include "darkritual.h"
#include "haste.h"
#include "magicfatigue.h"

class Deck {
  std::vector<std::shared_ptr<Card>> cardList;

public:
  Deck();
  void setDeck(const std::string file);
  void shuffle(int shift);
  std::shared_ptr<Card> draw();
};

#endif
