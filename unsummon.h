#ifndef _UNSUMMON_HEADER_
#define _UNSUMMON_HEADER_
#include "spell.h"

class Unsummon: public Spell {
public:
  Unsummon();
  std::string effect() override;
  std::string getDescription() override;
};

#endif
