#include "fireelemental.h"
using namespace std;

FireElemental:: FireElemental(): Minion{2, 2, 2, "Fire Elemental"} {}

string FireElemental:: effect() {
  return "summoneffect enemy damagesummon 1";
}

string FireElemental:: getDescription() {
  return "Whenever an opponent's minon enters play, deal 1 damage to it";
}
