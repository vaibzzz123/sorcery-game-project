#include "potionseller.h"
using namespace std;

PotionSeller:: PotionSeller(): Minion{1, 3, 2, "Potion Seller"} {}

string PotionSeller:: effect() {
  return "endturn friendly buffall 0 1";
}

string PotionSeller:: getDescription() {
  return "At the end of your turn, all your minions gain +0/+1";
}
