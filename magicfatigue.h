#ifndef _MAGICFATIGUE_HEADER_
#define _MAGICFATIGUE_HEADER_
#include "enchantment.h"

class MagicFatigue: public Enchantment {
public:
  MagicFatigue();
  int getEffectCost() override;
  std::string getDescription() override;
  std::string getTrueDescription() override;
};

#endif
