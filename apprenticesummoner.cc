#include "apprenticesummoner.h"
using namespace std;

ApprenticeSummoner:: ApprenticeSummoner(): Minion{1, 1, 1, "Apprentice Summoner"} {
  effectCost = 1;
}

string ApprenticeSummoner:: effect() {
  return "summon airelemental 1";
}

int ApprenticeSummoner:: getEffectCost() {
  return effectCost;
}

string ApprenticeSummoner:: getDescription() {
  return "Summon a 1/1 air elemental";
}
