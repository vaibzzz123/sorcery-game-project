#include "blizzard.h"
using namespace std;

Blizzard::Blizzard(): Spell{3, "Blizzard"} {}

string Blizzard:: effect() {
  return "damage all 2";
}

string Blizzard:: getDescription() {
  return "Deal 2 damage to all minions";
}
