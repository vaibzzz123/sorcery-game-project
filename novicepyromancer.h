#ifndef _NOVICEPYROMANCERL_HEADER_
#define _NOVICEPYROMANCERL_HEADER_
#include "minion.h"

class NovicePyromancer: public Minion {
  int effectCost;
public:
  NovicePyromancer();
  int getEffectCost() override;
  std::string effect() override;
  std::string getDescription() override;
};

#endif
