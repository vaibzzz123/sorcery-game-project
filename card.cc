#include "card.h"
using namespace std;

Card:: Card(): name{""}, type{""}, cost{0} {}

Card:: Card(string n, string t, int c) : name{n}, type{t}, cost{c} {}

string Card:: getName() {
  return name;
}

string Card:: getTrueName() {
  return name;
}

string Card:: getType() {
  return type;
}

void Card:: wrap(shared_ptr<Card> c) {}


shared_ptr<Card> Card:: getMinion() {
  return nullptr;
}

void Card:: setAction(int i) {}

void Card:: useAction() {}

int Card:: getAction() {
  return 0;
}

void Card:: setType(string s) {
  type = s;
}

int Card:: getCost() {
  return cost;
}

int Card:: getAtt() {
  return cost;
}

int Card:: getDef() {
  return cost;
}


void Card:: gainMana(int i) {}

int Card:: getEffectCost() {
  return 0;
}

int Card:: getTotalMana() {
  return 0;
}
void Card:: useEffect() {}

void Card:: damage(int i) {}

void Card:: setDef(int i) {}

void Card:: buffAtt(int i) {}

void Card:: buffDef(int i) {}

string Card:: effect() {
  return "";
}

void Card:: getAttacked(Card &c) {}

void Card:: attackMinion(Card &m) {}

string Card:: getDescription() {
  return "";
}

string Card:: getTrueDescription() {
  return "";
}
