#include "enrage.h"
using namespace std;

Enrage:: Enrage(): Enchantment{"Enrage", 2}, prevAttack{0}, prevDefense{0} {}

int Enrage:: getAtt() {
  if (getMinion() == nullptr) return 0;
  return getMinion()->getAtt() + prevAttack;
}

int Enrage:: getDef() {
  if (getMinion() == nullptr) return 0;
  return getMinion()->getDef() + prevDefense;
}

void Enrage:: wrap(shared_ptr<Card> c) {
  monster = c;
  if (c) {
    prevAttack = c->getAtt();
    prevDefense = c->getDef();
  }
}

string Enrage:: getDescription() {
  if (monster) return monster->getDescription();
  return "";
}

string Enrage:: getTrueDescription() {
  return "";
}
