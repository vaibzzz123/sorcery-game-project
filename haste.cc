#include "haste.h"
using namespace std;

Haste:: Haste(): Enchantment{"Haste", 1} {}

int Haste:: getAction() {
  if (monster) return monster->getAction() + 1;
  return 1;
}

string Haste:: getDescription() {
  if (monster) return monster->getDescription();
  return "Enchanted minion gains +1 action each turn";
}

string Haste:: getTrueDescription() {
  return "Enchanted minion gains +1 action each turn";
}
