#ifndef _APPRSUMMONER_HEADER_
#define _APPRSUMMONER_HEADER_
#include "minion.h"

class ApprenticeSummoner: public Minion {
  int effectCost;
public:
  ApprenticeSummoner();
  std::string effect() override;
  int getEffectCost() override;
  std::string getDescription() override;
};

#endif
