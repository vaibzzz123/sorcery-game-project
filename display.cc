#include "display.h"
using namespace std;
// change the lines of code that has comment starting with *
Display :: Display(shared_ptr<Battleground> bg, shared_ptr<Player> playerOne,
                   shared_ptr<Player> playerTwo, int active_player, int one,
                   int two, int inspect_minion_index): bg{bg}, playerOne{playerOne},
                   playerTwo{playerTwo}, active_player{active_player}, oneHealth{one},
                    twoHealth{two}, inspect_minion_index{inspect_minion_index} {}

card_template_t displayCard(shared_ptr<Card> card) {
  if (!card) return CARD_TEMPLATE_BORDER;
  card_template_t card_display; //string vector(card_template_t) for card display
  // Variables For all card
  string name = card->getName();
  string type = card->getType();
  string desc = card->getDescription();
  int cost = card->getCost();
  int attack = card->getAtt();
  int defence = card->getDef();
  int ability_cost = card->getEffectCost();
  int ritual_cost = card->getEffectCost();
  int ritual_charges = card->getTotalMana();
  // Card Type Control Flow
  if (type == "Minion") {
    if ((ability_cost != 0) && (desc != "")) {
      card_display = display_minion_activated_ability(name, cost, attack, defence, ability_cost, desc);
    } else if ((desc != "")) {
      card_display = display_minion_triggered_ability(name, cost, attack, defence, desc);
    } else {
      card_display = display_minion_no_ability(name, cost, attack, defence);
    }
  } else if (type == "Ritual") {
    card_display = display_ritual(name, cost, ritual_cost, desc, ritual_charges);
  } else if (type == "Spell") {
    card_display = display_spell(name, cost, desc);
  } else if (type == "Enchantment") {
    if (name == "Giant Strength") {
      card_display = display_enchantment_attack_defence(name, cost, desc, "+2", "+2");
    } else if (name == "Enrage") {
      card_display = display_enchantment_attack_defence(name, cost, desc, "*2", "*2");
    } else {
      card_display = display_enchantment(name, cost, desc);
    }
  }
  return card_display;
}

void Display :: displayHand() const {
  int hand_columns = 5;
  int card_height = CARD_TEMPLATE_BORDER.size();
  card_template_t hand(card_height);
  shared_ptr<Zone> hand_zone;
  if (active_player == 1) hand_zone = bg->getOneHand();
  if (active_player == 2) hand_zone = bg->getTwoHand();
  // makes Hand's rows
  for (int i = 0; i < hand_columns; i++) {
    auto card = hand_zone->getCard(i);
    card_template_t card_in_hand = displayCard(card);
    for (size_t j = 0; j < card_in_hand.size(); j++) {
      hand[j] += card_in_hand[j];
    }
  }
  // Prints Hand to std out
  for (auto i : hand) {
    cout << i << endl;
  }
}

string createBorders(int card_width, int board_columns, string top_bottom) {
  string border;
  int board_width = board_columns * card_width;
  if (top_bottom == "top") border.append(EXTERNAL_BORDER_CHAR_TOP_LEFT);
  else border.append(EXTERNAL_BORDER_CHAR_BOTTOM_LEFT);
  for (int i = 0; i < board_width; i++) border.append(EXTERNAL_BORDER_CHAR_LEFT_RIGHT);
  if (top_bottom == "top") border.append(EXTERNAL_BORDER_CHAR_TOP_RIGHT);
  else border.append(EXTERNAL_BORDER_CHAR_BOTTOM_RIGHT);
  return border;
}

void Display :: displayBoard() const {
  // Board  Dimention constants
  const int card_height = CARD_TEMPLATE_BORDER.size();
  const int card_width = 33;
  const int board_columns = 5;
  const int board_rows = 5;
  const int middle_design_row = 2;
  cout << createBorders(card_width, board_columns, "top") << endl;
  // Loops through the 4 rows that can hold cards on the board
  for (int z = 0; z < board_rows; z++) {
    card_template_t current_row(card_height); // card_template_t for current row
    card_template_t name_card;
    shared_ptr<Zone> board_zone;
    shared_ptr<Zone> grave_zone;
    if (z < middle_design_row) { // if in the top half rows of the board use player1 info(bg)
      board_zone = bg->getOneBoard();
      grave_zone = bg->getOneGrave();
      name_card = display_player_card(1,playerOne->getName(), oneHealth, playerOne->getMagic()); // u need gethealth
    } else if (z >= middle_design_row) { // if in the bottom half rows of the board use player2 info(bg)
      board_zone = bg->getTwoBoard();
      grave_zone = bg->getTwoGrave();
      name_card = display_player_card(2, playerTwo->getName(), twoHealth, playerTwo->getMagic());// u need gethealth
    }
    if (z % 2 == 1) {
      for (int i = 0; i < board_columns; i++) {
        auto card = board_zone->getCard(i);
        card_template_t card_in_hand = displayCard(card);
        for (size_t j = 0; j < card_in_hand.size(); j++) {
          current_row[j] += card_in_hand[j];
        }
      }
    } else if (z == middle_design_row) {
      current_row = CENTRE_GRAPHIC;
    } else {
      auto ritual_card = board_zone->getRit();
      auto grave_card = grave_zone->getRit();
      for (int i = 0; i < board_columns; i++) {
        card_template_t ritual_card_templcard_width = displayCard(ritual_card);
        card_template_t grave_card_templcard_width = displayCard(grave_card);
        for (size_t j = 0; j < current_row.size(); j++) {
          if (i == 0) current_row[j] += ritual_card_templcard_width[j];
          else if ((i == 1) || (i == 3)) current_row[j] += CARD_TEMPLATE_EMPTY[j];
          else if (i == 2) current_row[j] += name_card[j];
          else if (i == 4) current_row[j] += grave_card_templcard_width[j];
        }
      }
    }
    // Prints Board_cards to std out
    for (auto i : current_row) {
      if (z != 2) cout << EXTERNAL_BORDER_CHAR_UP_DOWN ;
      cout << i;
      if (z != 2) cout << EXTERNAL_BORDER_CHAR_UP_DOWN;
      cout << endl;
    }
  }
  cout << createBorders(card_width, board_columns, "bottom") << endl;
}

void Display :: displayMinion() const {
  const int card_height = CARD_TEMPLATE_BORDER.size();
  const int board_columns = 5;
  shared_ptr<Zone> board_zone;
  if (active_player == 1) {
    board_zone = bg->getOneBoard();
  } else if (active_player == 2) {
    board_zone = bg->getTwoBoard();
  }
  auto minion = board_zone->getCard(inspect_minion_index);
  card_template_t minion_template = displayCard(minion);
  for (int i = 1; i < board_columns; i++) {
    for (int j = 0; j < card_height; j++) {
      minion_template[j] += CARD_TEMPLATE_EMPTY[j];
    }
  }
  for (auto i : minion_template) cout << i << endl;
  shared_ptr<Card> enchantment_card = nullptr;
  if (enchantment_card) enchantment_card = minion->getMinion();
  while (enchantment_card) {
    card_template_t current_row(card_height);
    for (int i = 0; i < board_columns; i++) {
      card_template_t enchantment_template = displayCard(enchantment_card);
      for (int j = 0; j < card_height; j++) {
        if (enchantment_card) current_row[j] += enchantment_template[j];
        else current_row[j] += CARD_TEMPLATE_EMPTY[j];
      }
      if (enchantment_card) enchantment_card = enchantment_card->getMinion();
    }
    for (auto i : current_row) cout << i << endl;
  }
}
