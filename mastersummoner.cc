#include "mastersummoner.h"
using namespace std;

MasterSummoner:: MasterSummoner(): Minion{2, 3, 3, "Master Summoner"} {
  effectCost = 2;
}

string MasterSummoner:: effect() {
  return "summon airelemental 3";
}

int MasterSummoner:: getEffectCost() {
  return effectCost;
}

string MasterSummoner:: getDescription(){
  return "Summon up to three 1/1 air elementals";
}
