#include "enchantment.h"
using namespace std;

Enchantment:: Enchantment(string n, int c): Minion{0, 0, c, n}, monster{nullptr} {
  setType("Enchantment");
}

string Enchantment:: getName() {
  if (monster) return monster->getName();
  return name;
}

string Enchantment:: getTrueName() {
  return name;
}

string Enchantment:: getType() {
  if (monster) return monster->getType();
  return type;
}

int Enchantment:: getAtt() {
  if (monster) return monster->getAtt();
  return attack;
}

int Enchantment:: getDef() {
  if (monster) return monster->getDef();
  return defense;
}

int Enchantment:: getCost() {
  if (monster) return monster->getCost();
  return cost;
}

shared_ptr<Card> Enchantment:: getMinion() {
  return monster;
}

string Enchantment:: effect() {
  if (monster) return monster->effect();
  return "";
}

int Enchantment:: getEffectCost() {
  if (monster) return monster->getEffectCost();
  return 0;
}


void Enchantment:: setDef(int i) {
  if (monster) monster->setDef(i);
}

void Enchantment:: damage(int i) {
  if (monster) monster->damage(i);
}

void Enchantment:: buffDef(int i) {
  if (monster) monster->buffDef(i);
}

void Enchantment:: buffAtt(int i) {
  if (monster) monster->buffAtt(i);
}

void Enchantment:: getAttacked(Card &c) {
  c.attackMinion(*this);
}

void Enchantment:: attackMinion(Card &m) {
  damage(m.getAtt());
}

void Enchantment:: wrap(shared_ptr<Card> c) {
  monster = c;
}

void Enchantment:: setAction(int i) {
  if (monster) monster->setAction(i);
}

void Enchantment:: useAction() {
  if (monster) monster->useAction();
}

int Enchantment:: getAction() {
  if (monster) return monster->getAction();
  return 0;
}
