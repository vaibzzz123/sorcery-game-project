#include "unsummon.h"
using namespace std;

Unsummon:: Unsummon(): Spell{1, "Unsummon"} {}

string Unsummon:: effect() {
  return "return one";
}

string Unsummon:: getDescription() {
  return "Return target minion to its owner's hand";
}
