#include "ritual.h"
using namespace std;

Ritual:: Ritual(string name, int e, int t, int c): Card{name, "Ritual", c}, effectCost{e}, totalMana{t} {}

void Ritual:: gainMana(int i) {
  totalMana =  totalMana + i;
}

int Ritual:: getEffectCost() {
  return effectCost;
}

int Ritual:: getTotalMana() {
  return totalMana;
}

void Ritual:: useEffect() {
  if (effectCost <= totalMana) {
    totalMana = totalMana - effectCost;
  }
}
