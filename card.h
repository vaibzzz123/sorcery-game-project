#ifndef _CARD_HEADER_
#define _CARD_HEADER_
#include <string>
#include <iostream>
#include <memory>


class Card {
protected:
  std::string name;
  std::string type;
  int cost;
public:
  Card();
  Card(std::string n, std::string t, int c);
  virtual std::string getName();
  virtual std::string getTrueName();
  virtual std::string getType();
  virtual int getCost();
  virtual std::shared_ptr<Card> getMinion();
  virtual int getAtt();
  virtual int getDef();
  virtual void setDef(int i);
  void setType(std::string s);
  virtual void damage(int i);
  virtual void buffAtt(int i);
  virtual void buffDef(int i);
  virtual void gainMana(int i);
  virtual void useEffect();
  virtual int getEffectCost();
  virtual int getTotalMana();
  virtual std::string effect();
  virtual void getAttacked(Card &c);
  virtual void attackMinion(Card &m);
  virtual void wrap(std::shared_ptr<Card> c);
  virtual void setAction(int i);
  virtual void useAction();
  virtual int getAction();
  virtual std::string getDescription();
  virtual std::string getTrueDescription();
};
#endif
