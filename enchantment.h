#ifndef _ENCHANTMENT_HEADER_
#define _ENCHANTMENT_HEADER_
#include "minion.h"

class Enchantment: public Minion{
protected:
  std::shared_ptr<Card> monster;
public:
  Enchantment(std::string n, int c);
  std::shared_ptr<Card> getMinion() override;
  std::string getName() override;
  std::string getTrueName() override;
  std::string getType() override;
  int getCost() override;
  int getAtt() override;
  int getDef() override;
  int getEffectCost() override;
  void setDef(int i) override;
  void damage(int i) override;
  void buffDef(int i) override;
  void buffAtt(int i) override;
  void getAttacked(Card &c) override;
  void attackMinion(Card &m) override;
  std::string effect() override;
  void wrap(std::shared_ptr<Card> c) override;
  void setAction(int i) override;
  void useAction() override;
  int getAction() override;
};

#endif
