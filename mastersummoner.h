#ifndef _MASTERSUMMONER_HEADER_
#define _MASTERSUMMONER_HEADER_
#include "minion.h"

class MasterSummoner: public Minion {
  int effectCost;
public:
  MasterSummoner();
  std::string effect() override;
  int getEffectCost() override;
  std::string getDescription() override;
};

#endif
