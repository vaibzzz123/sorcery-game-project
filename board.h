#ifndef _BOARD_HEADER_
#define _BOARD_HEADER_
#include "zone.h"

class Board: public Zone {
    const int cap = 5;
    std::shared_ptr<Grid> rit;
  public:
    Board();
    void init() override;
    bool place(std::shared_ptr<Card> c) override;
    std::shared_ptr<Card> getRit() override;
    void removeRit() override;
};
#endif
