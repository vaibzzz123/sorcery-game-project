#include "zone.h"
using namespace std;

Zone:: Zone(): layout() {}

int Zone:: firstEmpty() const {
  int value = 0;
  for (auto i : layout) {
    if(i->isEmpty()) {
      break;
    }
    value++;
  }
  return value;
}

void Zone:: replace(shared_ptr<Card> c, int i) {
  layout[i]->placeCard(c);
}

bool Zone:: isFull() {
  for (auto i : layout) {
    if (i->isEmpty()) {
      return false;
    }
  }
  return true;
}

bool Zone:: place(shared_ptr<Card> c) {
  if (isFull()) {
    return false;
  }
  if (c == nullptr) {
    return false;
  }
  int index = firstEmpty();
  layout[index]->placeCard(c);
  return true;
}


shared_ptr<Card> Zone:: remove(int i) {
    int c = layout.size();
    if (i >= c) {
      cout << "out of index" << endl;
      return nullptr;
    }
    return layout[i]->removeCard();
}

shared_ptr<Card> Zone:: getCard(int i) {
  int c = layout.size();
  if (i >= c) {
    cout << "out of index" << endl;
    return nullptr;
  }
  return layout[i]->myCard();
}

int Zone:: getCount() {
  return layout.size();
}

shared_ptr<Card> Zone:: getRit() {return nullptr;}

void Zone:: removeRit() {}
