#include "grave.h"
using namespace std;

using namespace std;

Grave:: Grave(): Zone(), count{0} {}

void Grave:: init() {
  //???
}

int Grave:: getCount() const {
  return count;
}

bool Grave::place(shared_ptr<Card> c) {
  auto temp = make_shared<Grid>();
  temp->placeCard(c);
  layout.emplace_back(temp);
  count++;
  return true;
}

bool Grave:: isFull() {
  return false;
}

shared_ptr<Card> Grave:: remove(int i) {
  shared_ptr<Card> p = layout.back()->myCard();
  layout.pop_back();
  count--;
  return p;
}

shared_ptr<Card> Grave:: getRit() {
  if (count == 0) return nullptr;
  return layout.back()->myCard();
}
