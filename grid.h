#ifndef _GRID_HEADER_
#define _GRID_HEADER_
#include "card.h"
#include <memory>

class Grid{
  std::shared_ptr<Card> occ;
  bool placed;
public:
  Grid();
  void placeCard(std::shared_ptr<Card> c);
  std::shared_ptr<Card> removeCard();
  bool isEmpty();
  std::shared_ptr<Card> myCard();
};
#endif
