#ifndef _DARKRITUAL_HEADER_
#define _DARKRITUAL_HEADER_
#include "ritual.h"

class DarkRitual: public Ritual{
public:
  DarkRitual();
  std::string effect() override;
  std::string getDescription() override;
};

#endif
