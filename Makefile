CXX = g++-5
CXXFLAGS = -std=c++14 -Wall -MMD -Werror=vla
EXEC = sorcery
OBJECTS = actions.o airelemental.o apprenticesummoner.o ascii_graphics.o auraofpower.o banish.o battleground.o blizzard.o board.o bonegolem.o card.o darkritual.o deck.o disenchant.o display.o earthelemental.o enchantment.o enrage.o fireelemental.o giantstrength.o grave.o grid.o hand.o haste.o helpmessage.o magicfatigue.o main.o mastersummoner.o minion.o novicepyromancer.o player.o potionseller.o raisedead.o recharge.o ritual.o silence.o spell.o standstill.o turn.o unsummon.o zone.o
DEPENDS = ${OBJECTS:.o=.d}

${EXEC}: ${OBJECTS}
	${CXX} ${CXXFLAGS} ${OBJECTS} -o ${EXEC}

-include ${DEPENDS}

.PHONY: clean

clean:
	rm ${OBJECTS} ${EXEC} ${DEPENDS}
