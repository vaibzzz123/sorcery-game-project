#include <iostream>
using namespace std;
 
void printHelp() {
  cout << "Commands: ";
  cout << "help -- Display this message." << endl;
  cout << "end -- End the current players turn. " << endl;
  cout << "quit -- End the game. " << endl;
  cout << "attack minion other-minion -- Orders minion to attack other-minion." << endl;
  cout << "attack minion -- Orders minion to attack the opponent." << endl;
  cout << "play card [target-player target-card] -- Play card, optionally targeting target-card owned by target-player." << endl;
  cout << "use minion [target-player target-card] -- Use minions special ability, optionally targeting target-card owned by target-player." << endl;
  cout << "inspect minion -- View a minions card and all enchantments on that minion." << endl;
  cout << "hand -- Describe all cards in your hand." << endl;
  cout << "board -- Describe all cards on the board." << endl;
}
