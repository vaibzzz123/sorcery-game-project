#include "recharge.h"
using namespace std;

Recharge:: Recharge(): Spell{1, "Recharge"} {}

string Recharge:: effect() {
  return "ritual chargegain 3";
}

string Recharge:: getDescription() {
  return "Your ritual gains 3 charges";
}
