#ifndef _GRAVE_HEADER_
#define _GRAVE_HEADER_
#include "zone.h"

class Grave: public Zone {
  int count;
  public:
    Grave();
    void init() override;
    int getCount() const;
    bool isFull() override;
    bool place(std::shared_ptr<Card> c) override;
    std::shared_ptr<Card> remove(int i) override;
    std::shared_ptr<Card> getRit() override; // gets top card of grave
};

#endif
