#ifndef _SILENCE_HEADER_
#define _SILENCE_HEADER_
#include "enchantment.h"

class Silence: public Enchantment {
public:
  Silence();
  std::string effect() override;
  std::string getDescription() override;
  std::string getTrueDescription() override;
};

#endif
