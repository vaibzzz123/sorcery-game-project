#ifndef _AURAOP_HEADER_
#define _AURAOP_HEADER_
#include "ritual.h"

class AuraOfPower: public Ritual {
public:
  AuraOfPower();
  std::string effect() override;
  std::string getDescription() override;
};


#endif
