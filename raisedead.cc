#include "raisedead.h"
using namespace std;

RaiseDead:: RaiseDead(): Spell{1, "Raise Dead"} {}

string RaiseDead:: effect() {
  return "revive one";
}

string RaiseDead:: getDescription() {
  return "Resurrect the top minion in your graveyard and set its defense to 1";
}
