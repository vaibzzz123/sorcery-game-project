#ifndef _DISENCHANT_HEADER_
#define _DISENCHANT_HEADER_
#include "spell.h"

class Disenchant: public Spell {
public:
  Disenchant();
  std::string effect() override;
  std::string getDescription() override;
};

#endif
