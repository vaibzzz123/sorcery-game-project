#include "magicfatigue.h"
using namespace std;

MagicFatigue:: MagicFatigue(): Enchantment{"Magic Fatigue", 0} {}

int MagicFatigue:: getEffectCost() {
  if (monster) {
    if (monster->getEffectCost() == 0) return 0;
    return monster->getEffectCost() + 2;
  }
  return 0;
}

string MagicFatigue:: getDescription() {
  if (monster) return monster->getDescription();
  return "Enchanted minion's activated ability costs 2 more";
}

string MagicFatigue:: getTrueDescription() {
  return "Enchanted minion's activated ability costs 2 more";
}
