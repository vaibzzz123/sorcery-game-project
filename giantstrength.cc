#include "giantstrength.h"
using namespace std;

GiantStrength:: GiantStrength(): Enchantment{"Giant Strength", 1} {}

int GiantStrength:: getAtt() {
  if (getMinion() == nullptr) return 2;
  return getMinion()->getAtt() + 2;
}

int GiantStrength:: getDef() {
  if (getMinion() == nullptr) return 2;
  return getMinion()->getDef() + 2;
}

string GiantStrength:: getDescription() {
  if (monster) return monster->getDescription();
  return "";
}

string GiantStrength:: getTrueDescription() {
  return "";
}
