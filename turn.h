#ifndef _TURN_HEADER_
#define _TURN_HEADER_
#include <iostream>
#include "player.h"
#include "actions.h"
#include "battleground.h"
#include <sstream>

class Turn {
  int activePlayer;
  int oneHealth;
  int twoHealth;
  std::shared_ptr<Player> playerOne;
  std::shared_ptr<Player> playerTwo;
  std::shared_ptr<Battleground> field;
public:
  Turn(std::shared_ptr<Player> one, std::shared_ptr<Player> two, std::shared_ptr<Battleground> f);
  void drawCard();

  //triggers
  void startTurn();
  void startTrigger();
  void endTurn();
  void endTrigger();
  bool die(std::shared_ptr<Zone> z, int i, std::shared_ptr<Zone> grave);
  void dieTrigger();
  bool summon(std::shared_ptr<Card> c, int i);
  void summonTrigger(std::shared_ptr<Card> c);
  //triggers

  int active();
  int & getOneHealth();
  int & getTwoHealth();
  std::shared_ptr<Player> getPlayerOne();
  std::shared_ptr<Player> getPlayerTwo();
  std::shared_ptr<Battleground> getGround();
  void attackPlayer(int i);
  void attackMinion(int i, int j);
  void gameStart();
  bool nonTargetEffect(int i);
  bool targetEffect(int i, int p, int t);
  bool monsterTarget(int i, int p, int t);
  bool monsterNonTarget(int i);
  void judgementDay(std::shared_ptr<Zone> z, int i, std::shared_ptr<Zone> grave); // kills card at 0 hp
  void divineJudgement();
};

#endif
