#ifndef _BATTLE_HEADER_
#define _BATTLE_HEADER_
#include "board.h"
#include "grave.h"
#include "hand.h"
#include "zone.h"
#include <memory>

class Battleground {
  std::shared_ptr<Zone> oneHand;
  std::shared_ptr<Zone> oneBoard;
  std::shared_ptr<Zone> oneGrave;
  std::shared_ptr<Zone> twoHand;
  std::shared_ptr<Zone> twoBoard;
  std::shared_ptr<Zone> twoGrave;
public:
  Battleground();
  std::shared_ptr<Zone> getOneHand();
  std::shared_ptr<Zone> getOneBoard();
  std::shared_ptr<Zone> getOneGrave();
  std::shared_ptr<Zone> getTwoHand();
  std::shared_ptr<Zone> getTwoBoard();
  std::shared_ptr<Zone> getTwoGrave();
};
#endif
