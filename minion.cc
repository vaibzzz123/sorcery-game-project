#include "minion.h"
using namespace std;

Minion:: Minion(int att, int def, int c, string name): Card{name, "Minion", c}, attack{att}, defense{def}, action{0} {}

int Minion:: getAtt() {
  return attack;
}

int Minion:: getDef() {
  return defense;
}

void Minion:: setDef(int i) {
  defense = i;
}

void Minion:: damage(int i) {
  defense = defense - i;
}

void Minion:: buffDef(int i) {
  defense =  defense + i;
}

void Minion:: buffAtt(int i) {
  attack = attack + i;
}

void Minion:: getAttacked(Card &c) {
  c.attackMinion(*this);
}

void Minion:: attackMinion(Card &m) {
  damage(m.getAtt());
}

void Minion:: setAction(int i) {
  action = i;
}

void Minion:: useAction() {
  action--;
}

int Minion:: getAction() {
  return action;
}
